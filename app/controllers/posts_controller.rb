class PostsController < ApplicationController
  before_action :authenticate_user!
  before_action :correct_user, only: :destroy

  def show
    @post = Post.find(params[:id])
    @comment = Comment.new
    @user = User.find_by(id: @post.user_id)
  end

  def create
    @post = current_user.posts.build(post_params)
    if @post.save
      flash[:notice] = '投稿しました！'
      redirect_to root_url
    else
      @timeline = Post.order(created_at: :desc).paginate(page: params[:page], per_page: 20)
      flash.now[:alert] = '投稿に失敗しました。フォームに入力してください'
      render 'top/index'
    end
  end

  def destroy
    @post.destroy
    redirect_to request.referrer || root_url
  end

  private

  def post_params
    params.require(:post).permit(:content, :picture, :gurunabi_image)
  end

  def correct_user
    @post = current_user.posts.find_by(id: params[:id])
    redirect_to root_url if @post.nil?
  end
end
