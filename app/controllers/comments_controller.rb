class CommentsController < ApplicationController
  before_action :authenticate_user!

  def create
    @post = Post.find(params[:post_id])
    @comment = @post.comments.build(comment_params)
    @comment.user_id = current_user.id
    if @comment.save
      flash[:notice] = '投稿しました！'
      @post.create_notification_comment!(current_user, @comment.id)
      redirect_to post_path(@post)
    else
      @post = Post.find(params[:post_id])
      flash.now[:alert] = '投稿に失敗しました。フォームに入力してください'
      render 'posts/show'
    end
  end

  def destroy
    @comment = Comment.find_by(id: params[:id])
    @comment.destroy
    redirect_to  request.referrer || root_url
  end

  private

  def comment_params
    params.require(:comment).permit(:content)
  end
end
