class TopController < ApplicationController
  def index
    if user_signed_in?
      @post = current_user.posts.build
      user = User.find(current_user.id)
      follow_users = user.following
      timeline = Post.where(id: user.posts.last).or(Post.where(user_id: follow_users.ids))
      if follow_users.any?
        @timeline = timeline.order(created_at: :desc).paginate(page: params[:page], per_page: 40)
      else
        @timeline = Post.order(created_at: :desc).paginate(page: params[:page], per_page: 40)
      end
    end
  end
end
