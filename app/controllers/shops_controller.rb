class ShopsController < ApplicationController
  before_action :authenticate_user!
  def index
    api_key = ENV["GURUNABI_ID"]
    url = 'https://api.gnavi.co.jp/RestSearchAPI/v3/?keyid='
    url << api_key

    if params[:search]
      word = params[:search]
      url << "&freeword=" << word
    end

    url = URI.encode(url)
    uri = URI.parse(url)
    json = Net::HTTP.get(uri)
    result = JSON.parse(json)
    @rests = result["rest"]

    @post = current_user.posts.build
  end
end
