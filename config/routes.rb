Rails.application.routes.draw do
  devise_for :users, controllers: { registrations: 'users/registrations', sessions: 'users/sessions', omniauth_callbacks: 'users/omniauth_callbacks' }
  root "top#index"
  resources :users, only: %i(index show)
  get '/posts' => 'top#index'
  resources :posts, only: %i(show create destroy) do
    resources :comments, only: %i(create destroy)
  end
  resources :shops, only: %i(index)
  resources :users do
    member do
      get :following, :followers
    end
  end
  resources :relationships, only: %i(create destroy)
  resources :notifications, only: %i(index)
  devise_scope :user do
    post 'users/guest_sign_in', to: 'users/sessions#new_guest'
  end
end
