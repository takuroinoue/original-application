p "create Users"
User.create!(
  email: 'user1@example.com',
  password: 'aaaaaaaa',
  name: 'オリバー',
  self_introduction: 'オリバーです',
  sex: 0,
  image: open("#{Rails.root}/app/assets/images/seed_images1.jpg")
)
User.create!(
  email: 'user2@example.com',
  password: 'aaaaaaaa',
  name: 'ハリー',
  self_introduction: 'ハリーです',
  sex: 0,
  image: open("#{Rails.root}/app/assets/images/seed_images2.jpg")
)
User.create!(
  email: 'user3@example.com',
  password: 'aaaaaaaa',
  name: 'オリヴィア',
  self_introduction: 'オリヴィアです',
  sex: 1,
  image: open("#{Rails.root}/app/assets/images/seed_images3.jpg")
)
User.create!(
  email: 'user4@example.com',
  password: 'aaaaaaaa',
  name: 'リリー',
  self_introduction: 'リリーです',
  sex: 1,
  image: open("#{Rails.root}/app/assets/images/seed_images4.jpg")
)

50.times do |n|
  name = Faker::Name.name
  email = "example_#{n+1}@railstutorial.org"
  self_introduction = "#{n+1}号参上！"
  password = "password"
  User.create!(
    name: name,
    email: email,
    password: password,
    self_introduction: self_introduction,
    sex: 1,
    image: open("#{Rails.root}/app/assets/images/no-image.png")
  )
end

users = User.order(:created_at).take(5)
50.times do
  content = Faker::Lorem.sentence(6)
  users.each { |user| user.posts.create!(content: content) }
end

users = User.all
user  = users.first
following = users[2..50]
followers = users[3..40]
following.each { |followed| user.follow(followed) }
followers.each { |follower| follower.follow(user) }