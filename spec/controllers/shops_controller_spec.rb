require 'rails_helper'

RSpec.describe ShopsController, type: :request do
  before do
    @user = create(:user)
  end

  describe "#index" do
    it "成功時のレスポンスが返ってくること" do
      sign_in @user
      get shops_path
      expect(response).to have_http_status "200"
    end

    it "indexテンプレートがレンダリングされている" do
      sign_in @user
      get shops_path
      expect(response).to render_template :index
    end

    it "ログインしていないとログインページにレンダリングされる" do
      get shops_path
      expect(response).to have_http_status "302"
      expect(response).to redirect_to new_user_session_path
    end
  end
end
