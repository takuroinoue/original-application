require 'rails_helper'

RSpec.describe PostsController, type: :request do
  before do
    @user = create(:user)
    @post = create(:post, user_id: @user.id)
    sign_in @user
  end

  it "成功したレスポンスが返ってくる" do
    get post_path(@post.id)
    expect(response).to have_http_status "200"
  end
end
