require 'rails_helper'

RSpec.describe "UsersController", type: :request do
  before do
    @user = create(:user)
  end

  describe "#index" do
    it "成功時のレスポンスが返ってくること" do
      sign_in @user
      get users_path
      expect(response).to have_http_status "200"
    end

    it "indexテンプレートがレンダリングされている" do
      sign_in @user
      get users_path
      expect(response).to render_template :index
    end
  end

  describe "#show" do
    it "成功時のレスポンスが返ってくること" do
      sign_in @user
      get user_path(@user.id)
      expect(response).to have_http_status "200"
    end

    it "showテンプレートがレンダリングされている" do
      sign_in @user
      get user_path(@user.id)
      expect(response).to render_template :show
    end
  end

  describe "ログインしていない場合、ログインページにレンダリングされる" do
    it "#index" do
      get users_path
      expect(response).to have_http_status "302"
      expect(response).to redirect_to new_user_session_path
    end
    it "#show" do
      get user_path(@user.id)
      expect(response).to have_http_status "302"
      expect(response).to redirect_to new_user_session_path
    end
  end
end
