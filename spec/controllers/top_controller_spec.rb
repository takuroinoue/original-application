require 'rails_helper'

RSpec.describe TopController, type: :request do
  describe "#index" do
    it "成功時のレスポンスが返ってくること" do
      get root_path
      expect(response).to have_http_status "200"
    end
  end
end
