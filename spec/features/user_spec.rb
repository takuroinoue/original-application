require 'rails_helper'

RSpec.feature "Users", type: :feature do
  include Devise::Test::IntegrationHelpers
  before do
    @user = create(:user)
  end

  scenario "ログイン後、ログアウトできる" do
    visit root_path
    click_link "メールアドレスでログインする"
    expect(current_path).to eq new_user_session_path
    fill_in "emailを入力してください", with: @user.email
    fill_in "パスワードを入力してください", with: @user.password
    click_button "ログインする"
    expect(current_path).to eq root_path
    expect(page).to have_content 'ログインしました。'
    click_link "編集ページへ"
    expect(current_path).to eq user_path(@user.id)
    find('.User_delete').click
    expect(current_path).to eq root_path
    expect(page).to have_content 'ログアウトしました。'
  end

  scenario "Userの詳細が編集可能である" do
    sign_in @user
    visit edit_user_registration_path
    fill_in 'user_name', with: '井上拓郎'
    fill_in 'user_self_introduction', with: 'Railsの勉強しています！よろしくお願いします'
    attach_file "user[image]", "#{Rails.root}/spec/factories/images/itembox.jpg"
    click_button "更新"
    expect(current_path).to eq user_path(@user.id)
    expect(page).to have_content 'アカウント情報を変更しました。'
    expect(page).to have_content '井上拓郎'
    expect(page).to have_content 'Railsの勉強しています！よろしくお願いします'
    expect(page).to have_content @user.image
  end

  scenario "Showページにユーザー詳細が表示されている" do
    sign_in @user
    visit user_path(@user.id)
    expect(page).to have_content "Tommy"
    expect(page).to have_content "Hello, I am Tommy!"
  end

  feature "リンクが正しいリンク先である" do
    before do
      sign_in @user
      visit user_path(@user.id)
    end

    scenario "一覧へ戻る" do
      find('.User_List').click
      expect(current_path).to eq users_path
    end

    scenario "投稿する" do
      find('.User_post').click
      expect(current_path).to eq root_path
    end

    scenario "情報を編集" do
      find('.User_edit').click
      expect(current_path).to eq edit_user_registration_path
    end

    scenario "ログアウト" do
      find('.User_delete').click
      expect(current_path).to eq root_path
    end
  end
end
