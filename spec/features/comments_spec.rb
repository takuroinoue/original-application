require 'rails_helper'

RSpec.feature "Comments", type: :feature do
  include Devise::Test::IntegrationHelpers
  before do
    @user = create(:user)
    @post = create(:post, user_id: @user.id)
  end

  scenario "投稿個別ページに移動し、その後コメント出来て削除できる" do
    sign_in @user
    visit root_path
    expect(page).to have_content @post.content
    click_link 'コメントする'
    expect(current_path).to match post_path(@post.id)
    expect(page).to have_content @post.content
    fill_in 'comment_content', with: "僕も頑張ってますよ！"
    click_button "送信"
    expect(page).to have_content "投稿しました！"
    expect(page).to have_content "僕も頑張ってますよ！"
    find('.PostCommentsPage_right_delete').click
    expect(page).not_to have_content "僕も頑張ってますよ！"
  end
end
