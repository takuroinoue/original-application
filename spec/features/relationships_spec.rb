require 'rails_helper'

RSpec.feature "Relationships", type: :feature do
  include Devise::Test::IntegrationHelpers
  before do
    @user = create(:user)
    @second_user = create(:user, name: "Marry")
  end

  scenario "ユーザーは相手をフォローできて、フォロー覧に表示され, その後解除できる" do
    sign_in @user
    visit user_path(@second_user.id)
    expect(page).to have_content "Marry"
    expect(page).to have_content "フォロワー 0"
    click_button "フォロー"
    expect(page).to have_content "フォロワー 1"
    click_link "フォロワー 1"
    expect(current_path).to eq followers_user_path(@second_user.id)
    expect(page).to have_content @user.name
    click_link @user.name
    expect(current_path).to eq user_path(@user.id)
    click_link "フォロー中 1"
    expect(current_path).to eq following_user_path(@user.id)
    expect(page).to have_content @second_user.name
    click_link @second_user.name
    expect(current_path).to eq user_path(@second_user.id)
    expect(page).to have_content "フォロワー 1"
    click_button "フォロー中"
    expect(page).to have_content "フォロワー 0"
  end
end
