require 'rails_helper'

RSpec.feature "Notifications", type: :feature do
  include Devise::Test::IntegrationHelpers
  before do
    @user = create(:user)
    @second_user = create(:user, name: "Marry")
    @post = create(:post, user_id: @user.id)
  end

  scenario "通知がない場合、「お知らせ」と「通知一覧に通知」は表示されない" do
    sign_in @user
    visit root_path
    expect(page).not_to have_css '.notification-circle'
    find('.notification_page_link').click
    expect(current_path).to match notifications_path
    expect(page).to have_content "通知はありません"
  end

  scenario "他のユーザーがコメントした時、ユーザーに通知があること" do
    sign_in @second_user
    visit root_path
    expect(page).to have_content @post.content
    click_link 'コメントする'
    expect(current_path).to match post_path(@post.id)
    expect(page).to have_content @post.content
    fill_in 'comment_content', with: "僕も頑張ってますよ！"
    click_button "送信"
    expect(page).to have_content "投稿しました！"
    click_link "←投稿ページへ"
    expect(current_path).to match root_path
    click_link "編集ページへ"
    expect(current_path).to match user_path(@second_user.id)
    find('.User_delete').click
    # ユーザーを切り替える
    sign_in @user
    visit root_path
    # 通知のお知らせの表示
    expect(page).to have_css '.notification-circle'
    find('.notification_page_link').click
    expect(current_path).to match notifications_path
    expect(page).to have_content "Marry さんが あなたの投稿 にコメントしました"
  end

  scenario "他のユーザーがフォローした時、ユーザーに通知があること" do
    sign_in @second_user
    visit user_path(@user.id)
    expect(page).to have_content @user.name
    expect(page).to have_content "フォロワー 0"
    click_button "フォロー"
    expect(page).to have_content "フォロワー 1"
    click_link "Restaurant Share"
    click_link "編集ページへ"
    expect(current_path).to match user_path(@second_user.id)
    find('.User_delete').click
    # ユーザーを切り替える
    sign_in @user
    visit root_path
    # 通知のお知らせの表示
    expect(page).to have_css '.notification-circle'
    find('.notification_page_link').click
    expect(current_path).to match notifications_path
    expect(page).to have_content "Marry さんが あなたをフォローしました"
  end
end
