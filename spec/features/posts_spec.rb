require 'rails_helper'

RSpec.feature "Posts", type: :feature do
  include Devise::Test::IntegrationHelpers
  before do
    @user = create(:user)
  end

  scenario "投稿成功し、その後削除できる" do
    sign_in @user
    visit root_path
    expect(page).to have_content "Tommy"
    fill_in "Let's Share！ (200文字以内で入力してください)", with: '肉じゃがです！'
    attach_file "post[picture]", "#{Rails.root}/spec/factories/images/itembox.jpg"
    click_button '送信'
    expect(page).to have_content "肉じゃがです！"
    expect(page).to have_css 'li.Timeline_Image'
    expect(page).to have_content "削除する"
    click_link '削除する'
    expect(page).not_to have_content "肉じゃがです！"
    expect(page).not_to have_css 'li.Timeline_Image'
  end
end
