FactoryBot.define do
  factory :comment do
    content "可愛いね！"
    association :post
    user { post.user }
  end
end
