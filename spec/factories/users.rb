FactoryBot.define do
  factory :user do
    name "Tommy"
    sequence(:email) { |n| "tester#{n}@example.com" }
    password "aaaaaa"
    self_introduction "Hello, I am Tommy!"
    sex 0
  end
end
