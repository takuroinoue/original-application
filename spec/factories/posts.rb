FactoryBot.define do
  factory :post, :class => "Post" do
    content "hello!"
    association :user, factory: :user
  end

  factory :other_post do
    content "こんにちは！"
    association :user, factory: :other_user
  end
end
