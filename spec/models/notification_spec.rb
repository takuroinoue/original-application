require 'rails_helper'

RSpec.describe Notification, type: :model do
  before do
    @user = create(:user)
    @other_user = create(:user, name: "Arrey")
    @notification = Notification.new(visitor_id: @user.id, visited_id: @other_user.id)
  end

  it "visitor_idがないと無効である" do
    @notification.visitor_id = nil
    @notification.valid?
  end

  it "visited_idがないと無効である" do
    @notification.visited_id = nil
    @notification.valid?
  end
end
