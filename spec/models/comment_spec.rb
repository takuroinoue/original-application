require 'rails_helper'

RSpec.describe Comment, type: :model do
  it "有効なファクトリを持つこと" do
    expect(create(:comment)).to be_valid
  end

  it "紐付いているUserが等しいこと" do
    user = create(:user)
    comment = build(:comment, user_id: user.id)
    expect(comment.user).to eq user
  end

  it "紐付いているPostが等しいこと" do
    post = create(:post)
    comment = build(:comment, post_id: post.id)
    expect(comment.post).to eq post
  end

  it "Userに紐づいたデータである必要がある" do
    comment = build(:comment, user_id: nil)
    comment.valid?
  end

  it "Postに紐づいたデータである必要がある" do
    comment = build(:comment, post_id: nil)
    comment.valid?
  end

  it "コメントがなければ無効であること" do
    comment = build(:comment, content: nil)
    comment.valid?
    expect(comment.errors[:content]).to include("を入力してください")
  end
end
