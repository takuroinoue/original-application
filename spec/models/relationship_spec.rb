require 'rails_helper'

RSpec.describe Relationship, type: :model do
  before do
    @user = create(:user)
    @other_user = create(:user, name: "Arrey")
    @relationship = Relationship.new(follower_id: @user.id, followed_id: @other_user.id)
  end

  it "follower_idがないと無効である" do
    @relationship.follower_id = nil
    @relationship.valid?
    expect(@relationship.errors[:follower_id]).to include("を入力してください")
  end

  it "followed_idがないと無効である" do
    @relationship.followed_id = nil
    @relationship.valid?
    expect(@relationship.errors[:followed_id]).to include("を入力してください")
  end
end
