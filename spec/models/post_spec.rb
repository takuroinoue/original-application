require 'rails_helper'

RSpec.describe Post, type: :model do
  subject(:post) { build(:post, user: user) }

  let(:user) { create(:user) }

  # 有効なファクトリを持つこと
  it { should be_valid }

  it "正しいユーザーである" do
    expect(post.user).to eq user
  end

  describe "validations" do
    describe "存在性" do
      it { is_expected.to validate_presence_of :user_id }
      it { is_expected.to validate_presence_of :content }
    end

    describe "文字数" do
      it { should validate_length_of(:content).is_at_most(200) }
    end
  end
end
