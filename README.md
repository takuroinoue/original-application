この度はこのアプリケーションを見て頂きありがとうございます。
このアプリはお気に入りのレストランを検索したり、投稿できます。
何故このアプリを作ったか、と言いますと、前職が長年調理師をやっていて、その知識を何か活かせないかというふうに考え、このようなSNSを作ることにしました。
スクールの学びを生かすべく、開発環境にDockerを取り入れ、CircleclCIでRubocopのコードチェックやRspec、herokuへの自動デプロイを含めて開発を行いました。
初めての経験ばかりで見苦しい点も多々あると思いますが、どうぞよろしくお願いいたします。

================= 開発の際のコマンドメモ ======================

* Railsコマンド実行
 docker-compose run web rails ...

* down後、再度データベースを作成
 docker-compose run web rake db:create

* Rubocop実行
 bundle exec rubocop --require rubocop-airbnb
 * Rubocop自動修正
 bundle exec rubocop -a

* Rspec実行
 bin/rspec

 * BitbucketにPushする
 git push -u origin master
 git push https://takuroinoue@bitbucket.org/takuroinoue/original-application.git ブランチ名


* circleclci/config.ymlをチェックする
 circleci config validate